package it.bytesize.transcoder;

import java.util.List;

public class ApiResponse<T> {
    private List<T> data;
    private String status;

    public ApiResponse(List<T> data, String status) {
        this.data = data;
        this.status = status;
    }

    public List<T> getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }
}
