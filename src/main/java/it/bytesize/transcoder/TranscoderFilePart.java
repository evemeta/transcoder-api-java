package it.bytesize.transcoder;

import java.util.Date;
import java.util.UUID;

public interface TranscoderFilePart {
//    UUID       uuid.UUID `json:"uuid"`
//    FileSize   int64     `json:"size"`
//    FileName   string    `json:"name"`
//    Added      time.Time `json:"creationDate"`
//    RangeStart int64     `json:"rangeStart"`
    UUID getUUID();
    int getFileSize();
    String getName();
    Date getCreateDate();
    long getRangeStart();
}
