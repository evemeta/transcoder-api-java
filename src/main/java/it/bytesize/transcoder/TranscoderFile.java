package it.bytesize.transcoder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface TranscoderFile {

    UUID getUUID();
    int getFileSize();
    String getDirectory();
    String getName();
    Date getCreateDate();
    int getUserId();
    boolean shouldDeleteAfterDownload();
    boolean isAvailable();
    List<TranscoderFilePart> getFileParts();

}
