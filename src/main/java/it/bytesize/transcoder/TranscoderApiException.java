package it.bytesize.transcoder;

public class TranscoderApiException extends RuntimeException {

    private final ApiResponse<String> response;

    public TranscoderApiException(ApiResponse<String> response) {
        super(response.getData().get(0));
        this.response = response;
    }

    public ApiResponse<String> getResponse() {
        return response;
    }
}
