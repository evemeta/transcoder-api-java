package it.bytesize.transcoder.impl;

import it.bytesize.transcoder.TranscoderFile;
import it.bytesize.transcoder.TranscoderFilePart;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TranscoderFileImpl implements TranscoderFile {

    private UUID uuid;
    private int size;
    private String name;
    private String dir;
    private String hash;
    private Date creationDate;
    private int userId;
    private boolean deleteAfterDownload;
    private boolean isAvailable;
    private List<TranscoderFilePart> parts;


    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public int getFileSize() {
        return size;
    }

    @Override
    public String getDirectory() {
        return dir;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getHash() {
        return hash;
    }

    @Override
    public Date getCreateDate() {
        return creationDate;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public boolean shouldDeleteAfterDownload() {
        return deleteAfterDownload;
    }

    @Override
    public boolean isAvailable() {
        return isAvailable;
    }

    @Override
    public List<TranscoderFilePart> getFileParts() {
        return parts;
    }
}
