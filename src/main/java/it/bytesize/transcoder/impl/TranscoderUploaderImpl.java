package it.bytesize.transcoder.impl;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import it.bytesize.transcoder.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.FileAlreadyExistsException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TranscoderUploaderImpl implements TranscoderUploader {
    private final static String FILE_INFO_API_ENDPOINT = Transcoder.API_BASE_URL + "v1/browse/search?apiKey=%s&fileName=%s&dir=%s";
    private final static String FILE_UPLOAD_API_ENDPOINT = Transcoder.API_BASE_URL + "v1/upload/chunks?apiKey=%s";

    private CloseableHttpClient httpClient = HttpClients.createDefault();
    private Gson gsonInstance = new GsonBuilder()
            .registerTypeAdapter(TranscoderFile.class, InterfaceSerializer.interfaceSerializer(TranscoderFileImpl.class))
            .registerTypeAdapter(TranscoderFilePart.class, InterfaceSerializer.interfaceSerializer(TranscoderFilePartImpl.class))
            .create();
    private final String apiKey;

    public TranscoderUploaderImpl(String apiKey) {
        this.apiKey = apiKey;
    }

    private TranscoderFilePart checkPart(TranscoderFile file, long startRange, long endRange){
        endRange -= 1;
        for (TranscoderFilePart part : file.getFileParts()) {
            long partRangeEnd = part.getRangeStart() + part.getFileSize() - 1;
            if (startRange >= part.getRangeStart() && startRange <= partRangeEnd) {
                return part;
            } else if (endRange >= part.getRangeStart() && endRange <= partRangeEnd) {
                return part;
            } else if (part.getRangeStart() <= startRange && endRange <= part.getRangeStart()) {
                return part;
            } else if (startRange >= part.getRangeStart() && endRange <= partRangeEnd) {
                return part;
            } else if (startRange <= part.getRangeStart() && endRange >= partRangeEnd) {
                return part;
            }
        }
        return null;
    }

    @Override
    public TranscoderFile getFileInfo(String fileName, String dir) {
        try {
            HttpGet request = new HttpGet(String.format(FILE_INFO_API_ENDPOINT, apiKey, fileName, dir));
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                HttpEntity entity = response.getEntity();
                int responseCode = response.getStatusLine().getStatusCode();
                if (responseCode == 200) {
                    if (entity != null) {
                        Type t = new TypeToken<ApiResponse<TranscoderFile>>() {}.getType();
                        ApiResponse<TranscoderFile> apiResponse = gsonInstance.fromJson(new InputStreamReader(entity.getContent()), t);
                        if (apiResponse.getData().size() != 1) {
                            throw new RuntimeException("there is no file matching this parameters");
                        }
                        return apiResponse.getData().get(0);
                    }
                } else if (responseCode == 404) {
                    return null;
                } else if (responseCode == 401) {
                    throw new InvalidApiKeyException();
                }else{
                    Type t = new TypeToken<ApiResponse<String>>() {}.getType();
                    ApiResponse<String> apiResponse = gsonInstance.fromJson(new InputStreamReader(entity.getContent()), t);
                    throw new TranscoderApiException(apiResponse);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("unable to retrieve file details", e);
        }
        return null;
    }

    private void checkFileArgument(File file) throws FileNotFoundException {
        if(file == null){
            throw new IllegalArgumentException("given file is null");
        }

        if(!file.exists()){
            throw new FileNotFoundException();
        }

        if(!file.isFile()){
            throw new IllegalArgumentException("unsupported file type, directories are not supported");
        }
    }

    private String generateHash(File file) throws IOException {
        long length = 4096;
        if(file.length() < length){
            length = file.length();
        }

        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) length];
        fis.read(data);

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(data);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return null;
    }

    @Override
    public void upload(File file, String dir, int chunkSize) throws FileNotFoundException, FileAlreadyExistsException {
        checkFileArgument(file);

        if("".equals(dir)){
            dir = "/";
        }

        TranscoderFile fileInfo = getFileInfo(file.getName(), dir);
        if(fileInfo != null && fileInfo.isAvailable()){
            throw new FileAlreadyExistsException("this file already exists and cannot be modified");
        }

        long fileSize = file.length();
        long start = 0;
        long end = chunkSize;

        while(end <= fileSize){
            if(fileInfo != null){
                TranscoderFilePart filePart = checkPart(fileInfo, start, end);
                while (filePart != null && end <= fileSize){
                    if (start < filePart.getRangeStart() && end > (start + filePart.getFileSize())){
                        end = filePart.getRangeStart();
                    }else{
                        start += filePart.getFileSize();
                        end = start + chunkSize;
                        if (end > fileSize) {
                            end = fileSize;
                        }
                    }
                    filePart = checkPart(fileInfo, start, end);
                }
            }

            uploadPart(file, dir, (int)start, (int)(end - start));

            start = end;
            if (start >= fileSize) {
                break;
            }
            end = start + chunkSize;
            if (end >= fileSize) {
                end = fileSize;
            }
        }
    }

    @Override
    public void uploadPart(File file, String dir, int offset, int length) throws FileNotFoundException {
        checkFileArgument(file);

        if("".equals(dir)){
            dir = "/";
        }

        try {
            HttpPost request = new HttpPost(String.format(FILE_UPLOAD_API_ENDPOINT, apiKey));
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("fileName", new StringBody(file.getName(), ContentType.MULTIPART_FORM_DATA));
            builder.addPart("fileSize", new StringBody(String.valueOf(file.length()), ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dir", new StringBody(dir, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("start", new StringBody(String.valueOf(offset), ContentType.MULTIPART_FORM_DATA));

            try (FileInputStream fis = new FileInputStream(file)){
                byte[] data = new byte[length];
                fis.skip(offset);
                fis.read(data);
                builder.addBinaryBody("file", data, ContentType.DEFAULT_BINARY, String.format("%s.part", file.getName()));
            }

            request.setEntity(builder.build());
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                HttpEntity entity = response.getEntity();
                int responseCode = response.getStatusLine().getStatusCode();
                switch (responseCode){
                    case 200:
                        break;
                    default:
                        Type t = new TypeToken<ApiResponse<String>>() {}.getType();
                        ApiResponse<String> apiResponse = gsonInstance.fromJson(new InputStreamReader(entity.getContent()), t);
                        throw new TranscoderApiException(apiResponse);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("unable to retrieve file details", e);
        }
    }

    private static final class InterfaceSerializer<T> implements JsonSerializer<T>, JsonDeserializer<T> {

        private final Class<T> implementationClass;

        private InterfaceSerializer(final Class<T> implementationClass) {
            this.implementationClass = implementationClass;
        }

        static <T> InterfaceSerializer<T> interfaceSerializer(final Class<T> implementationClass) {
            return new InterfaceSerializer<>(implementationClass);
        }

        @Override
        public JsonElement serialize(final T value, final Type type, final JsonSerializationContext context) {
            final Type targetType = value != null
                    ? value.getClass() // `type` can be an interface so Gson would not even try to traverse the fields, just pick the implementation class
                    : type;            // if not, then delegate further
            return context.serialize(value, targetType);
        }

        @Override
        public T deserialize(final JsonElement jsonElement, final Type typeOfT, final JsonDeserializationContext context) {
            return context.deserialize(jsonElement, implementationClass);
        }

    }
}
