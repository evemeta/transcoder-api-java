package it.bytesize.transcoder.impl;

import it.bytesize.transcoder.TranscoderFilePart;

import java.util.Date;
import java.util.UUID;

public class TranscoderFilePartImpl implements TranscoderFilePart {

    private UUID uuid;
    private int size;
    private String name;
    private Date creationDate;
    private long rangeStart;

    public TranscoderFilePartImpl(UUID uuid, int size, String name, Date creationDate, long rangeStart) {
        this.uuid = uuid;
        this.size = size;
        this.name = name;
        this.creationDate = creationDate;
        this.rangeStart = rangeStart;
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public int getFileSize() {
        return size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Date getCreateDate() {
        return creationDate;
    }

    @Override
    public long getRangeStart() {
        return rangeStart;
    }
}
