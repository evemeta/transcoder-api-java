package it.bytesize.transcoder;

import it.bytesize.transcoder.impl.TranscoderUploaderImpl;

public class Transcoder {
    public final static String API_BASE_URL = "http://localhost:8081/";

    public static TranscoderUploader createUploader(String apiKey){
        return new TranscoderUploaderImpl(apiKey);
    }
}
