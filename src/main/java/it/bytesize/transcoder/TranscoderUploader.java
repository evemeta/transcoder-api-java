package it.bytesize.transcoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;

public interface TranscoderUploader {
    TranscoderFile getFileInfo(String fileName, String dir);
    void upload(File file, String dir, int chunkSize) throws FileNotFoundException, FileAlreadyExistsException;
    void uploadPart(File file, String dir, int offset, int length) throws FileNotFoundException;
}
