package it.bytesize.transcoder;

public class InvalidApiKeyException extends RuntimeException {
    public InvalidApiKeyException() {
        super("Invalid api key");
    }
}
